<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ReviewsStat;
use Auth;

class Reviews extends Model
{
    protected $table = 'reviews';
    protected $fillable = [
        'user', 'star', 'text', 'date', 'role', 'brand_id', 'anonymous', 'purchased_products', 'i_purchased', 'i_purchased_products', 'use_to_describe', 'good_value_for_money', 'leader_in_their_product', 'eco_friendly', 'using_website', 'establishments', 'love_about_brend', 'dislike_about_brend', 'i_would_refer', 'i_would_shop', 'radio_rating', 'privacy_policy', 'title_of_your_review', 'remain_anonymous', 'name_of_your_company', 'company_address', 'city', 'state', 'country', 'postcode', 'phone_number', 'website', 'first_name', 'position_in_company', 'how_many_years', 'do_you_supply_directly', 'which_product_categories_do', 'file', 'contents_of_the_uploaded_document', 'fair_price_and_payment_terms', 'planning', 'сommunication', 'timely_payment', 'quality_of_partnership', 'tell_us_more_about_your_experience', 'what_could_do_better', 'could_do_that_would_surprise_you', 'i_certify_that_the_information', 'status', 'geo',
    ];

    public static function add ($data)
    {
        if($data->file){ $path = $data->file('file')->store('files', 'public'); $path = '/storage/' . $path; }else{ $path = ''; }
        if($data->anonymous){ $anon = 'yes'; }else{ $anon = 'no'; }
        if(Auth::check()){ $user = Auth::user()->id; }else{ $user = 'anonim'; }
        if($data->role == 'supplier'){ $status = -1; }else{ $status = 0; }

        $ip = $_SERVER['REMOTE_ADDR'];
        $geo = file_get_contents('http://ip-api.com/php/'.$ip);
        $geo = unserialize($geo);

        // var_dump($geo);
        // exit;

    	Self::create([
    		'user' => $user,
    		'star' => $data->star,
    		'text' => $data->text,
    		'date' => time(),
    		'role' => $data->role,
    		'brand_id' => $data->brand,
            'anonymous' => $anon,
            'purchased_products' => $data->purchased_products,
            'i_purchased' => $data->i_purchased,
            'i_purchased_products' => $data->i_purchased_products,
            'use_to_describe' => $data->use_to_describe,
            'good_value_for_money' => $data->good_value_for_money,
            'leader_in_their_product' => $data->leader_in_their_product,
            'eco_friendly' => $data->eco_friendly,
            'using_website' => $data->using_website,
            'establishments' => $data->establishments,
            'love_about_brend' => $data->love_about_brend,
            'dislike_about_brend' => $data->dislike_about_brend,
            'i_would_refer' => $data->i_would_refer,
            'i_would_shop' => $data->i_would_shop,
            'radio_rating' => $data->radio_rating,
            'privacy_policy' => $data->privacy_policy,
            'title_of_your_review' => $data->title_of_your_review,
            'remain_anonymous' => $data->remain_anonymous,
            'name_of_your_company' => $data->name_of_your_company,
            'company_address' => $data->company_address,
            'city' => $data->city,
            'state' => $data->state,
            'country' => $data->country,
            'postcode' => $data->postcode,
            'phone_number' => $data->phone_number,
            'website' => $data->website,
            'first_name' => $data->first_name,
            'position_in_company' => $data->position_in_company,
            'how_many_years' => $data->how_many_years,
            'do_you_supply_directly' => $data->do_you_supply_directly,
            'which_product_categories_do' => $data->which_product_categories_do,
            'file' => $path,
            'contents_of_the_uploaded_document' => $data->contents_of_the_uploaded_document,
            'fair_price_and_payment_terms' => $data->fair_price_and_payment_terms,
            'planning' => $data->planning,
            'сommunication' => $data->сommunication,
            'timely_payment' => $data->timely_payment,
            'quality_of_partnership' => $data->quality_of_partnership,
            'tell_us_more_about_your_experience' => $data->tell_us_more_about_your_experience,
            'what_could_do_better' => $data->what_could_do_better,
            'could_do_that_would_surprise_you' => $data->could_do_that_would_surprise_you,
            'i_certify_that_the_information' => $data->i_certify_that_the_information,
            'status' => $status,
            'geo' => $geo['country'],
    	]);

        $stat = ReviewsStat::where('brand_id', $data->brand)->first();
        if($stat){
            if($data->role == 'consumer'){
                $rate = $stat->star_customer + $data->radio_rating;
                $rate = $rate / 2;
                $stat->star_customer = $rate;
                $stat->save();
            }else{

                $rate = $data->fair_price + $data->planning + $data->communication + $data->timely_payment + $data->quality_of_partnership + $stat->star_supplier;
                $rate = $rate / 6;
                $stat->star_supplier = $rate;
                $stat->save();

            }
        }else{
            if($data->role == 'consumer'){
                ReviewsStat::create([
                    'star_customer' => $data->radio_rating,
                    'star_supplier' => 0,
                    'brand_id' => $data->brand
                ]);
            }else{
                $rate = $data->fair_price + $data->planning + $data->communication + $data->timely_payment + $data->quality_of_partnership;
                $rate = $rate / 5;
                ReviewsStat::create([
                    'star_customer' => 0,
                    'star_supplier' => $rate,
                    'brand_id' => $data->brand
                ]);
            }
        }
    }
}
