<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    protected $table = 'brands';
    protected $fillable = [
        'name', 'country', 'city', 'address', 'www', 'fb', 'ig', 'tw', 'url',
    ];

    public static function filter ($data)
    {
    	$results = Self::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier');
    	
    	if(isset($data->country)){
    		$results = $results->whereIn('brands.country', $data->country);
    	}

        if(isset($data->likes)){
            $results = $results
                ->where(
                    function ($query) use ($data) {
                        if(isset($data->likes['one']))
                            $query = $query->orWhere('reviews_stat.star_customer', '>=', 1)->where('reviews_stat.star_customer', '<=', 1.99);

                        if(isset($data->likes['two']))
                            $query = $query->orWhere('reviews_stat.star_customer', '>=', 2)->where('reviews_stat.star_customer', '<=', 2.99);

                        if(isset($data->likes['three']))
                            $query = $query->orWhere('reviews_stat.star_customer', '>=', 3)->where('reviews_stat.star_customer', '<=', 3.99);

                        if(isset($data->likes['four']))
                            $query = $query->orWhere('reviews_stat.star_customer', '>=', 4)->where('reviews_stat.star_customer', '<=', 4.99);

                        if(isset($data->likes['five']))
                            $query = $query->orWhere('reviews_stat.star_customer', '>=', 5);

                        return $query;
                    }
                );
        }

        if(isset($data->stars)){
            $results = $results
                ->where(
                    function ($query) use ($data) {
                        if(isset($data->stars['one']))
                            $query = $query->orWhere('reviews_stat.star_supplier', '>=', 1)->where('reviews_stat.star_supplier', '<=', 1.99);

                        if(isset($data->stars['two']))
                            $query = $query->orWhere('reviews_stat.star_supplier', '>=', 2)->where('reviews_stat.star_supplier', '<=', 2.99);

                        if(isset($data->stars['three']))
                            $query = $query->orWhere('reviews_stat.star_supplier', '>=', 3)->where('reviews_stat.star_supplier', '<=', 3.99);

                        if(isset($data->stars['four']))
                            $query = $query->orWhere('reviews_stat.star_supplier', '>=', 4)->where('reviews_stat.star_supplier', '<=', 4.99);

                        if(isset($data->stars['five']))
                            $query = $query->orWhere('reviews_stat.star_supplier', '>=', 5);

                        return $query;
                    }
                );
        }

        if(isset($data->sort))
        {
            switch ($data->sort) {
                case 1:
                    $results = $results->orderBy('brands.id', 'desc');
                    break;
                case 2:
                    $results = $results->orderBy('reviews_stat.star_customer', 'desc');
                    break;
                case 3:
                    $results = $results->orderBy('reviews_stat.star_supplier', 'desc');
                    break;
                case 4:
                    $results = $results->orderBy('reviews_stat.star_customer', 'asc');
                    break;
                case 5:
                    $results = $results->orderBy('reviews_stat.star_supplier', 'asc');
                    break;
            }
        }

    	$results = $results->get();
    	return $results;
    }

    public static function getCountLikes ($like)
    {
        $min = $like;
        $max = $like + 0.99;

        $results = Self::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')
            ->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')
            ->where('reviews_stat.star_customer', '>=', $min)
            ->where('reviews_stat.star_customer', '<=', $max)
            ->count();

        return $results;
    }

    public static function getCountStars ($star)
    {
        $min = $star;
        $max = $star + 0.99;

        $results = Self::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')
            ->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')
            ->where('reviews_stat.star_supplier', '>=', $min)
            ->where('reviews_stat.star_supplier', '<=', $max)
            ->count();

        return $results;
    }
}
