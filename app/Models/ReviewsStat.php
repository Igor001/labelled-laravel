<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ReviewsStat extends Model
{
    protected $table = 'reviews_stat';
    protected $fillable = [
        'star_customer', 'star_supplier', 'brand_id',
    ];
}
