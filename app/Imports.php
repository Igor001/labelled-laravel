<?php

namespace App;

use App\Imports\Brands;
use Maatwebsite\Excel\Concerns\FromCollection;

class Imports implements FromCollection
{
    public function collection()
    {   
        return SalesOrder::all();
    }
}
