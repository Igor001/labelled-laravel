<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brands as BrModel;
use App\Models\Reviews;
use App\Models\ReviewsStat;
use Auth;
use App\Core;

class SiteController extends Controller
{
    public function brends (){
        $results = BrModel::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')->paginate(10);
        $brends = BrModel::groupBy('country')->get();
        $returnHTML = view('web.brend-list', compact('results'))->render();
        
        return view('web.brend', compact('returnHTML', 'brends'));
    }

    public function showMore (Request $request)
    {
        $results = BrModel::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')->paginate(10);
        $i = 0;
        foreach ($results as $key) {
            $i++;
        }
        $returnHTML = view('web.brend-list', compact('results'))->render();
        return response()->json(array('status' => true, 'html' => $returnHTML, 'count' => $i));
    }

    public function customerReviews ($brand)
    {
        $brand = BrModel::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')->where('brands.name', $brand)->first();
        $lists = Reviews::where('brand_id', $brand->id);
        $count = $lists->count();
        $lists = $lists->get();    
        return view('web.customerReviews', compact('brand', 'lists', 'count'));
    }

    public function search (Request $request)
    {
    	$q = $request->q;

    	// Если поиск равен пустому значению
    	if(!$q){
    		$results = BrModel::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')->get();
    		$returnHTML = view('web.brend-list', compact('results'))->render();
    		return response()->json(array('status' => true, 'html' => $returnHTML, 'count' => 0, 'q' => $q));
    	}

    	// Поиск результат на соответсвие
    	$results = BrModel::leftJoin('reviews_stat', 'brands.id', '=', 'reviews_stat.brand_id')->select('brands.*', 'reviews_stat.star_customer', 'reviews_stat.star_supplier')->where('brands.name', 'like', '%'. $q .'%');
    	// Количество найденых результатов
    	$count = $results->count();
    	// Представление списка найденых резльтатов в масив
    	$results = $results->get();
    	// Обработка результата поиска
    	$returnHTML = view('web.brend-list', compact('results'))->render();
    	// Возврат обработаных в HTML брендов по поиску
    	// Создание json масива для работы с jquery и Live обработчиком
    	return response()->json(array('status' => true, 'html' => $returnHTML, 'count' => $count, 'q' => $q));
    }

    public function filter (Request $request)
    {
        $results = BrModel::filter($request);
        $returnHTML = view('web.brend-list', compact('results'))->render();

        return response()->json(array('status' => true, 'html' => $returnHTML));
    }

    public function customerReviewsAdd (Request $request)
    {
        Reviews::add($request);
        return redirect()->back();
    }

    public function getName ()
    {
        $user = Auth::user();

        return response()->json(array('status' => true, 'name' => $user->name));
    }

    public function writeReviewBrand ($brand)
    {
        $brand = BrModel::where('name', $brand)->first();
        return view('web.write-review', compact('brand'));
    }

    public function writeReview ()
    {
        return view('web.write-review');
    }

    public function writeReviewSearch (Request $request)
    {
        if($request->name){
            $results = BrModel::Where('name', 'like', '%'. $request->name .'%')->get();
            $returnHTML = view('web.write-review-search-list', compact('results'))->render();
        }else {
            $returnHTML = '';
        }
        $res = BrModel::Where('name', $request->name)->first();
        if($res){
            $val = true;
        }else{
            $val = false;
        }
        return response()->json(array('status' => true, 'html' => $returnHTML, 'val' => $val));
    }

    public function getBlockReview (Request $request)
    {
        $result = BrModel::where('id', $request->id)->first();
        $brand = $result;
        $returnHTML = view('web.write-review-box', compact('result', 'brand'))->render();
        return response()->json(array('status' => true, 'html' => $returnHTML));
    }

    public function homeSearch (Request $request)
    {
        $results = BrModel::Where('name', 'like', '%'. $request->name .'%')->get();
        $returnHTML = view('web.write-review-search-list-home', compact('results'))->render();
        return response()->json(array('status' => true, 'html' => $returnHTML));
    }

    public function getModalLogin (Request $request)
    {
        $role = $request->role;
        $action = $request->action;
        $method = $request->method;
        $returnHTML = view('layouts.modal-step', compact('action', 'role', 'method'))->render();
        return response()->json(array('status' => true, 'html' => $returnHTML));
    }

    public function setModalLogin(Request $request)
    {
        if($request->core == 'login')
        {
            $html = Core::getModalLoginList();
        }else{
            $html = Core::getModalRegisterList();
        }

        return response()->json(array('status' => true, 'html' => $html));
    }
}
