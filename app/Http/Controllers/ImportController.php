<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Excel;
use App\Models\BrandImport;
use App\Imports\Brands;
use App\Models\Brands as BrModel;

class ImportController extends Controller
{
    public function import (){
    	return view('web.output');
    }

    public function importSave (Request $request){
    	
    	$rows = Excel::toArray(new Brands, $request->file('file')); 

    	foreach ($rows[0] as $key => $value) {
    		if($value[0] == 'NAME'){ continue; }
    		if(!$value[0] OR $value[0] == null){ continue; }
    		
    		BrModel::create([
    			'name' => $value[0],
    			'country' => $value[1],
    			'city' => $value[2],
    			'address' => $value[3],
    			'www' => $value[4],
    			'fb' => $value[5],
    			'ig' => $value[6],
    			'tw' => $value[7],
    			'url' => $value[8], 
    		]);
    	}

        return redirect()->back();
    }
}
