<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use App\User;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Hash;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function google_auth ($role)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip = str_replace('.', '', $ip);
        setcookie($ip, $role, time()+3600, '/');
        return Socialite::with('google')->redirect();
    }

    public function google_callback ()
    {
        $user = Socialite::with('google')->stateless()->user();
        
        // Find user in DB.
        $userData = User::where('email', $user->email)->first();

        // Check exist user.
        if (isset($userData->id)) {

            // Make login user.
            Auth::loginUsingId($userData->id, TRUE);
            \Session::flash('flash_message_error', trans('interface.AccountNotActive'));

            $name = $userData->name;
            $html = "<script>window.opener.document.getElementById(\"header-sign\").innerHTML = '<a href=\"/home\" class=\"header-sign_sign-up\">$name</a>'</script>";
            $html .= "<script>document.getElementById('authCheck').value='auth';</script>";
            $html .= "<script>window.close();</script>";
            echo $html;
            //return redirect()->back();
        }
        // Make registration new user.
        else {

            $ip = $_SERVER['REMOTE_ADDR'];
            $ip = str_replace('.', '', $ip);
            $role = $_COOKIE[$ip];
            setcookie($ip, $role, -1, '/');

            if(!isset($role)){ $role = 'user'; }

            // Create new user in DB.
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make('auth_google_pass'),
                'role' => $role
            ]);

            // Make login user.
            Auth::loginUsingId($newUser->id, TRUE);

            \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

            $name = $newUser->name;
            $html = "<script>window.opener.document.getElementById(\"header-sign\").innerHTML = '<a href=\"/home\" class=\"header-sign_sign-up\">$name</a>'</script>";
            $html .= "<script>document.getElementById('authCheck').value='auth';</script>";
            $html .= "<script>window.close();</script>";
            echo $html;
            // return redirect()->route('home');
        }
    }

    public function linkedin_auth ($role)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip = str_replace('.', '', $ip);
        setcookie($ip, $role, time()+3600, '/');
        return Socialite::driver('linkedin')->redirect();
    }

    public function linkedin_callback ()
    {
        $user = Socialite::driver('linkedin')->stateless()->user();
        
        // Find user in DB.
        $userData = User::where('email', $user->email)->first();

        // Check exist user.
        if (isset($userData->id)) {

            // Make login user.
            Auth::loginUsingId($userData->id, TRUE);

            \Session::flash('flash_message_error', trans('interface.AccountNotActive'));
            
            $name = $userData->name;
            $html = "<script>window.opener.document.getElementById(\"header-sign\").innerHTML = '<a href=\"/home\" class=\"header-sign_sign-up\">$name</a>'</script>";
            $html .= "<script>document.getElementById('authCheck').value='auth';</script>";
            $html .= "<script>window.close();</script>";
            echo $html;
            //return redirect()->back();
        }
        // Make registration new user.
        else {

            $ip = $_SERVER['REMOTE_ADDR'];
            $ip = str_replace('.', '', $ip);
            $role = $_COOKIE[$ip];
            setcookie($ip, $role, -1, '/');

            if(!isset($role)){ $role = 'user'; }

            // Create new user in DB.
            $newUser = User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => Hash::make('auth_google_pass'),
                'role' => $role
            ]);

            // Make login user.
            Auth::loginUsingId($newUser->id, TRUE);

            \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

            $name = $newUser->name;
            $html = "<script>window.opener.document.getElementById(\"header-sign\").innerHTML = '<a href=\"/home\" class=\"header-sign_sign-up\">$name</a>'</script>";
            $html .= "<script>document.getElementById('authCheck').value='auth';</script>";
            $html .= "<script>window.close();</script>";
            echo $html;
            // return redirect()->route('home');
        }

    }

    public function facebook_auth ($role)
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        $ip = str_replace('.', '', $ip);
        setcookie($ip, $role, time()+3600, '/');
        return Socialite::driver('facebook')->redirect();
    }

    public function facebook_callback ()
    {
        $user = Socialite::driver('facebook')->stateless()->user();

        $email = $user->name . '@' . $_SERVER['SERVER_NAME'];
        
        // Find user in DB.
        $userData = User::where('email', $email)->first();

        // Check exist user.
        if (isset($userData->id)) {

            // Make login user.
            Auth::loginUsingId($userData->id, TRUE);

            \Session::flash('flash_message_error', trans('interface.AccountNotActive'));

            $name = $userData->name;
            $html = "<script>window.opener.document.getElementById(\"header-sign\").innerHTML = '<a href=\"/home\" class=\"header-sign_sign-up\">$name</a>'</script>";
            $html .= "<script>document.getElementById('authCheck').value='auth';</script>";
            $html .= "<script>window.close();</script>";
            echo $html;
            // return redirect()->back();
        }
        // Make registration new user.
        else {

            $ip = $_SERVER['REMOTE_ADDR'];
            $ip = str_replace('.', '', $ip);
            $role = $_COOKIE[$ip];
            setcookie($ip, $role, -1, '/');

            if(!isset($role)){ $role = 'user'; }
            // if($user->email){ $email = $user->email; }else{ $email = $user->name . '@' . $_SERVER['SERVER_NAME']; }

            // Create new user in DB.
            $newUser = User::create([
                'name' => $user->name,
                'email' => $email,
                'password' => Hash::make('auth_google_pass'),
                'role' => $role
            ]);

            // Make login user.
            Auth::loginUsingId($newUser->id, TRUE);

            \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

            $name = $newUser->name;
            $html = "<script>window.opener.document.getElementById(\"header-sign\").innerHTML = '<a href=\"/home\" class=\"header-sign_sign-up\">$name</a>'</script>";
            $html .= "<script>document.getElementById('authCheck').value='auth';</script>";
            $html .= "<script>window.close();</script>";
            echo $html;
            // return redirect()->route('home');
        }
    }
}
