<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Core extends Model
{
    public static function getModalLoginList ()
    {
    	$action = 'list';
    	$method = 'login';
    	$returnHTML = view('layouts.modal-step', compact('action', 'method'))->render();
        // return response()->json(array('status' => true, 'html' => $returnHTML));
        return $returnHTML;
    }

    public static function getModalRegisterList ()
    {
    	$action = 'list';
    	$method = 'register';
    	$returnHTML = view('layouts.modal-step', compact('action', 'method'))->render();
        // return response()->json(array('status' => true, 'html' => $returnHTML));
        return $returnHTML;
    }
}
