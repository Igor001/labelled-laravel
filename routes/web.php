<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/brands', 'SiteController@brends')->name('brends');
// Отзывы
Route::get('/reviews/{brand}', 'SiteController@customerReviews')->name('customerReviews');
Route::post('/reviews/{brand}', 'SiteController@customerReviewsAdd')->name('customerReviews.add');

Route::get('/import', 'ImportController@import')->name('import.view');
Route::post('/import', 'ImportController@importSave')->name('import');

// Поиск по названию для ajax GET
Route::get('/q', 'SiteController@search')->name('search');
// Поиск по названию для ajax GET на главной
Route::get('/home/q', 'SiteController@homeSearch')->name('home.search');
// Фильтры на странице брендов
Route::post('/filter', 'SiteController@filter')->name('filter');

// Auth Socialite
// ---
// ---
// AUTH google
Route::get('/auth/google/{role}', 'Auth\LoginController@google_auth')->name('google.auth');
Route::get('/google/callback', 'Auth\LoginController@google_callback')->name('google.callback');
// AUTH linkedin
Route::get('/auth/linkedin/{role}', 'Auth\LoginController@linkedin_auth')->name('linkedin.auth');
Route::get('/linkedin/callback', 'Auth\LoginController@linkedin_callback')->name('linkedin.callback');
// AUTH facebook
Route::get('/auth/facebook/{role}', 'Auth\LoginController@facebook_auth')->name('facebook.auth');
Route::get('/facebook/callback', 'Auth\LoginController@facebook_callback')->name('facebook.callback');
// ---
// ---

// Gen name acc
Route::get('/get-name', 'SiteController@getName')->name('get.name');
// Write a review
Route::get('/write-a-review', 'SiteController@writeReview')->name('write.review');
Route::get('/write-a-review-search', 'SiteController@writeReviewSearch')->name('write.review.search');
Route::get('/write-a-review/{brand}', 'SiteController@writeReviewBrand')->name('write.review.brand');


// Get Block Review
Route::get('/get/block-review', 'SiteController@getBlockReview')->name('get.block.review');

// Get Block Review
Route::get('/get/show-more', 'SiteController@showMore')->name('get.show.more');

// Get modal content LOGIN
Route::get('/get/modal-login', 'SiteController@getModalLogin')->name('get.modal.login');
Route::get('/set/modal-login', 'SiteController@setModalLogin')->name('set.modal.login');

