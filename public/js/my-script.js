$(document).on('click', ".reset-filter", function() {
	$('#filter_form input:checked').trigger('click')
});

$(".block-sort_by").click(function() {
	$(this).toggleClass('active');
	$(this).find('.drop-down_wrap').toggle(function(){
		$(this).find('.drop-down_wrap').slideDown();
	})
});

jQuery(function($){
  $(document).mouseup(function (e){
    var div = $(".block-sort_by.active");
      if (!div.is(e.target)
        && div.has(e.target).length === 0) {
        div.removeClass('active');
        div.find('.drop-down_wrap').slideUp();
      }
  });
});

$(".drop-index").click(function() {
	var name = $(this).text();
	$(this).parents(".block-sort_by").find('.rename_id').html(name);
});

$(document).on('click', ".consumer-bt", function() {
	$('.box-for-choice_button').removeClass('active');
	$(this).addClass('active');
	$('.form-consumer').addClass('active');
	$('.form-supplier').removeClass('active');
});
$(document).on('click', ".supplier-bt", function() {
	$('.box-for-choice_button').removeClass('active');
	$(this).addClass('active');
	$('.form-consumer').removeClass('active');
	$('.form-supplier').addClass('active');
});
$(".box-for-choice_button").click(function() {
	$('#anonymous').prop('checked', false);
	$('#remain-anonymous').prop('checked', false);
	$('.index-step_last').removeClass('mkls');
	$('.index-step_3').show();
});

$('.index-lb-rat').click(function() {
	var ide = $(this).attr('title');
	$(this).parents('.overall-rating-area').find('.count-overall-rating').html(ide);
});

$(document).ready(function() {
	$('#anonymous, #remain-anonymous').change(function() {
	    if(this.checked) {
	       $('.index-step_3').hide();
	       $('.index-step_last').addClass('mkls');
	    }else {
	       $('.index-step_3').show();
	       $('.index-step_last').removeClass('mkls');
	    }
	})
});


