var myWindow;

$(document).on('input', '.search_inp', function(){
	var example = '159 results containing “Va”'; // #results_text
	var q = $(this).val();

	// Запускаем анимацию загрузки
	$('#result_box').css('display', 'none');
	$('#result_box').html('');
	$('#loading_box').css('display', 'block');

	// Проверка на пустоту поиска
	if($(this).val() == ''){
		$('#results_text').html('');
		$('#loading_box').css('display', 'none');
        $('#result_box').css('display', 'block');
	}

	// Отправляем запрос на сервер для получение результат по поиску
	$.ajax({
        type: 'GET',
        url: '/q?q=' + q,
        success: function(data){
        	// выводим в html результат поиска
            $('#result_box').html(data['html']);
            // генерируем и выводим поисковый запрос
        	// Проверяем на пустоту значений и пробелы
        	if(data['q'] != null){
        		$('#results_text').html(data['count'] + ' results containing "' + data['q'] + '"');
        	}
            // Скрываем анимацию загрузки
            // Открываем html результата
            $('#loading_box').css('display', 'none');
            $('#result_box').css('display', 'block');
			
        }
    });
});

$(document).on('change', '.filter-country, .filter-likes, .filter-stars', function(e){
    // Запускаем анимацию загрузки
    $('#result_box').css('display', 'none');
    $('#result_box').html('');
    $('#loading_box').css('display', 'block');

    e.preventDefault();
    var formData = new FormData(document.getElementById("filter_form"));

    console.log(formData);

    $.ajax({
        type:'POST', // Тип запроса
        url: '/filter', // Скрипт обработчика
        data: formData, // Данные которые мы передаем
        cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
        contentType: false, // Тип кодирования данных мы задали в форме, это отключим
        processData: false, // Отключаем, так как передаем файл
        success:function(data){
            $('#result_box').html(data['html']);

            // Скрываем анимацию загрузки
            // Открываем html результата
            $('#loading_box').css('display', 'none');
            $('#result_box').css('display', 'block');
        },
        error:function(data){
            alert('Filter error.');
        }
    });
})

$(document).on('click', '.sort-action', function(e){
    var sort = $(this).data('sort');
    $('#sortData').val(sort);

    // Запускаем анимацию загрузки
    $('#result_box').css('display', 'none');
    $('#result_box').html('');
    $('#loading_box').css('display', 'block');

    e.preventDefault();
    var formData = new FormData(document.getElementById("filter_form"));

    console.log(formData);

    $.ajax({
        type:'POST', // Тип запроса
        url: '/filter', // Скрипт обработчика
        data: formData, // Данные которые мы передаем
        cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
        contentType: false, // Тип кодирования данных мы задали в форме, это отключим
        processData: false, // Отключаем, так как передаем файл
        success:function(data){
            $('#result_box').html(data['html']);

            // Скрываем анимацию загрузки
            // Открываем html результата
            $('#loading_box').css('display', 'none');
            $('#result_box').css('display', 'block');
        },
        error:function(data){
            alert('Filter error.');
        }
    });
})

// Register consumer
$(document).on('submit', '#reg_consumer', function(e){
    $('#reg_error_consumer_email').html('');
    $('#reg_error_consumer_password').html('');
    e.preventDefault();
    var formData = new FormData(document.getElementById("reg_consumer"));

    $.ajax({
        type:'POST', // Тип запроса
        url: '/register', // Скрипт обработчика
        data: formData, // Данные которые мы передаем
        cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
        contentType: false, // Тип кодирования данных мы задали в форме, это отключим
        processData: false, // Отключаем, так как передаем файл
        success:function(data){
            console.log(data);
            $('#close_consumer').trigger('click');
            $('#good_modal').trigger('click');
            getName();
        },
        error:function(data){
            $('#reg_error_consumer_email').html('Mail entered in the wrong format');
            $('#reg_error_consumer_password').html('Minimum 8 characters');
        }
    });
})
// Register Supplier
$(document).on('submit', '#reg_supplier', function(e){
    $('#reg_error_supplier_email').html('');
    $('#reg_error_supplier_password').html('');
    e.preventDefault();
    var formData = new FormData(document.getElementById("reg_supplier"));

    $.ajax({
        type:'POST', // Тип запроса
        url: '/register', // Скрипт обработчика
        data: formData, // Данные которые мы передаем
        cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
        contentType: false, // Тип кодирования данных мы задали в форме, это отключим
        processData: false, // Отключаем, так как передаем файл
        success:function(data){
            $('#close_supplier').trigger('click');
            $('#good_modal').trigger('click');
            getName();
        },
        error:function(data){
            $('#reg_error_supplier_email').html('Mail entered in the wrong format');
            $('#reg_error_supplier_password').html('Minimum 8 characters');
        }
    });
})
// Register Brand
$(document).on('submit', '#reg_brand', function(e){
    $('#reg_error_brand_email').html('');
    $('#reg_error_brand_password').html('');
    e.preventDefault();
    var formData = new FormData(document.getElementById("reg_brand"));

    $.ajax({
        type:'POST', // Тип запроса
        url: '/register', // Скрипт обработчика
        data: formData, // Данные которые мы передаем
        cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
        contentType: false, // Тип кодирования данных мы задали в форме, это отключим
        processData: false, // Отключаем, так как передаем файл
        success:function(data){
            $('#close_brand').trigger('click');
            $('#good_modal').trigger('click');
            getName();
        },
        error:function(data){
            $('#reg_error_brand_email').html('Mail entered in the wrong format');
            $('#reg_error_brand_password').html('Minimum 8 characters');
        }
    });
})

function getName ()
{
    $.ajax({
        type: 'GET',
        url: '/get-name',
        success: function(data){
            $('.header-sign').html('<a href="/home" class="header-sign_sign-up">'+ data['name'] +'</a>');
        }
    });
}

$(document).on('input', '#qReview', function(){
    var q = $(this).val();

    // Отправляем запрос на сервер для получение результат по поиску
    $.ajax({
        type: 'GET',
        url: '/write-a-review-search?name=' + q,
        success: function(data){
            if (data['val'] == true) {
                $('.review-box-c-c').css('display', 'block');
            }else {
                $('.review-box-c-c').css('display', 'none');
            };
            $('.help-box_wrap').html(data['html']);
        }
    });
});

$(document).on('click', '.list-item_index', function() {
    if($(this).hasClass('list-item-target')){ return false; }
    var field = $(this).text();
    var id = $(this).data('id');
    console.log(field);
    $(this).parents('.box-for-inp-search_help').find('#qReview').val(field);
    $(this).parents('.box-for-inp-search_help').find('.modal-help-block').html('');
    setBlockReview(id);
});

function setBlockReview (id)
{
    $.ajax({
        type: 'GET',
        url: '/get/block-review?id=' + id,
        success: function(data){
            $('.review-box-c-c').html(data['html']);
            $('.review-box-c-c').css('display', 'block');
        }
    });   
}

$(document).on('input', '#qHome', function(){
    var q = $(this).val();

    // Отправляем запрос на сервер для получение результат по поиску
    $.ajax({
        type: 'GET',
        url: '/home/q?name=' + q,
        success: function(data){
            $('.qHome_cls').html(data['html']);
        }
    });
});

$(document).on('click', '.list-item-target', function(){
    var url = $(this).data('name');
    var field = $(this).text();
    $(this).parents('.box-for-inp-search_help').find('#qHome').val(field);
    $(this).parents('.box-for-inp-search_help').find('.modal-help-block').html('');
    $('#urlBrandGo').val(url);
});

$(document).on('click', '.mb-search-button', function(){
    var url = $('#urlBrandGo').val();
    if(url){
        location = '/reviews/' + url;
    }
})

$(document).on('click', '#showMore', function(){
    var page = parseInt($('#current-page').val());
    var newPage = page + 1;
    $('#current-page').val(newPage);

    $.ajax({
        type: 'GET',
        url: '/get/show-more?page=' + newPage,
        success: function(data){
            $('#result_box').append(data['html']);
            if(data['count'] < 10){ $('#showMore').parents('.show-more-brends').remove(); }
        }
    });    
})

$(document).on('submit', '.form-consumer', function(){
    var check = $('#authCheck').val();
    if(check == 'no-auth'){
        if(!$('.form-consumer').find('#anonymous').is(":checked")){
            // var destination = $('.form-consumer').find('#anonymous').offset().top;
            // jQuery("html:not(:animated),body:not(:animated)").animate({
            //   scrollTop: destination
            // }, 800);
            $('.modal-rg-consumer_bt').trigger('click');
            return false;
        }
    }
})

$(document).on('submit', '.form-supplier', function(){
    var check = $('#authCheck').val();
    if(check == 'no-auth'){
        if(!$('.form-supplier').find('#remain-anonymous').is(":checked")){
            // var destination = $('.form-supplier').find('#anonymous').offset().top;
            // jQuery("html:not(:animated),body:not(:animated)").animate({
            //   scrollTop: destination
            // }, 800);
            $('.modal-rg-supplier_bt').trigger('click');
            return false;
        }
    }
})

function openNewWindow (url)
{
    var d = document.documentElement;
    var h = 500,
        w = 500;
    var myWindow = window.open(url, 'myWindow', 'scrollbars=1,height='+Math.min(h, screen.availHeight)+',width='+Math.min(w, screen.availWidth)+',left='+Math.max(0, ((d.clientWidth - w)/2 + window.screenX))+',top='+Math.max(0, ((d.clientHeight - h)/2 + window.screenY)));
    $('.close').trigger('click');
}

function setModalLogin (role, action, method)
{
    $.ajax({
        type: 'GET',
        url: '/get/modal-login?role=' + role + '&action=' + action+'&method='+method,
        success: function(data){
            $('#modal-login-result').html(data['html']);
        }
    });
}

function setModalLoginContent (core)
{
    $.ajax({
        type: 'GET',
        url: '/set/modal-login?core=' + core,
        success: function(data){
            $('#modal-login-result').html(data['html']);
        }
    });
}

