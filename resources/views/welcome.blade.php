@extends('layouts.app')

@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="js/addons/rating.js"></script>
    <section class="main-block">
        <div class="container">
            <div class="main-block-content">
                <div class="main-block-content_h1">
                    <h1>Verified Reviews of <span class="mbc-h1_blue">Fashion Companies</span> at your fingertips</h1>
                </div>
                <div class="main-block-content_p">
                    <p>Find Reviews on Fashion Brands and start buying now:</p>
                </div>
                <div class="main-block-search">
                    <form action="#" method="post" autocomplete="off">
                        <div class="main-block-search_group">
                            <div class="mb-search_group-block">
                                <div class="box-for-inp-search_help">
                                <input type="search" placeholder="Enter name of Business or product" class="search_inp" name="email" id="qHome" autocomplete="off">
                                <input type="hidden" id="urlBrandGo" name="">
                                <button type="button" value="" class="mb-search-button mb-search-button-go">go</button>
                                    <div class="help-box_wrap qHome_cls">

                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection