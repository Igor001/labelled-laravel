<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}" defer></script> -->

    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	
    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style1.css') }}">
</head>
<body>
    <input type="hidden" name="checkAuth" id="authCheck" value="{{ (Auth::check())? 'auth' : 'no-auth' }}">
    <header>
        <nav class="navbar navbar-expand-lg header">
            <div class="container">
                <div class="header-logo">
                    <a href="{{ route('welcome') }}">
                        <img src="{{ asset('img/logo.svg') }}" alt="">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li><a href="{{ route('brends') }}" class="cls-menu-lnk {{ Request::is('brends') ? 'active' : '' }}">Search</a></li>
                        <li><a href="{{ route('write.review') }}" class="cls-menu-lnk {{ Request::is('write-a-review*') ? 'active' : '' }}">Write a review</a></li>
                        <li><a href="#" class="cls-menu-lnk">For Brands</a></li>
                        <li><a href="#" class="cls-menu-lnk">Our Services</a></li>
                    </ul>
                </div>
                <div class="dis-flex-center">
                    @guest
                    <div class="header-sign" id="header-sign">
                        <img class="icon-usr_m" src="{{ asset('img/user.svg') }}">
                        <button type="button" class="cls-menu-lnk login-menu" data-toggle="modal" data-target="#form1" onclick="setModalLoginContent('login');">Login</button> <span class="separator_m">/</span> 
                        <a href="javascript:void(0);" class="cls-menu-lnk link-menu_s-u" data-toggle="modal" data-target="#form1" onclick="setModalLoginContent('register');">Register</a>

                        
                        @include('layouts.modal')

                    </div>
                    @else
                    <div class="header-sign" id="header-sign">
                        <a href="{{ route('home') }}" class="header-sign_sign-up">{{ Auth::user()->name }}</a>
                    </div>
                    @endguest
                    <button class="navbar-toggler mb-btn-r collapsed" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="true" aria-label="Toggle navigation">
                        <div class="strp-menu str_1"></div>
                        <div class="strp-menu str_2"></div>
                        <div class="strp-menu str_3"></div>
                    </button>
                </div>
            </div>
        </nav>
    </header>

    @yield('content')

    <footer class="footer">
        <div class="container">
            <div class="footer-top">
                <div class="footer-top-section">
                    <h3 class="footer-title">About</h3>
                    <ul>
                        <li><a href="#">About Us</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="footer-top-section">
                    <h3 class="footer-title">Discover</h3>
                    <ul>
                        <li><a href="{{ route('write.review') }}" class="{{ Request::is('write-a-review*') ? 'active' : '' }}">Write a review</a></li>
                        <li><a href="{{ route('brends') }}" class="{{ Request::is('brends') ? 'active' : '' }}">Search for Brands</a></li>
                        <li><a href="javascript:void(0);" data-toggle="modal" data-target="#form1" onclick="setModalLoginContent('register');">Sign Up</a></li>
                    </ul>
                </div>
                <div class="footer-top-section">
                    <h3 class="footer-title">The Benefits of P2P Reviews</h3>
                    <ul>
                        <li><a href="#">Our Advantage</a></li>
                        <li><a href="#">Market your Brand</a></li>
                    </ul>
                </div>
                <div class="footer-top-section">
                    <h3 class="footer-title">Subscribe</h3>
                    <form action="#" method="post" class="footer-top-subscribe-form">
                        <div class="footer-top-subscribe-form_input">
                            <input type="email" placeholder="Your email" class="form-input_email" name="email" id="">
                            <button type="submit" value=""><img src="{{ asset('img/left-arrow.png') }}" alt="left-arrow"></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="footer-line">
            </div>
            <div class="footer-bottom">
                <div class="footer-bottom_left">
                    <p>&copy; {{ date('Y') }} Labelled. All rights reserved.</p>
                </div>
                <div class="footer-bottom_right">
                    <p>Privacy Policy | Terms and Conditions</p>
                </div>
            </div>
        </div>
    </footer>
    <button type="submit" class="btn primary modal-reg-f-button modal-rg-consumer_bt" data-toggle="modal" data-target="#consumerModal">Submit review</button>
    <button type="submit" class="btn primary modal-reg-f-button modal-rg-supplier_bt" data-toggle="modal" data-target="#supplierModal">Submit review</button>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        $('.mb-search_group-block input').bind({
            focusin: function() {
                $('.select-block-m').css('display', 'flex');
            },
            focusout: function() {
                $('.select-block-m').css('display', 'none');
            }
        });
    </script>

    <script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/my-script.js') }}"></script>

</body>
</html>
