@if($action == 'list')
  <div class="modal-header" style="padding-bottom: 30px">
    <div class="modal-header_img">
      <img src="{{ asset('img/logo.svg') }}">
    </div>
    @if($method == 'login')
    	<h2 class="modal-title" id="exampleModalLabel" style="text-align: left;">Log into your account</h2>
    @else
    	<h2 class="modal-title" id="exampleModalLabel" style="text-align: left;">Register</h2>
    @endif
    <p class="move_text">Are you a consumer, supplier or a brand?</p>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
 
  <div class="registr_button" onclick="setModalLogin('consumer', '{{ $method }}', '{{ $method }}');">
    <img src="{{ asset('img/icon-worker-blue.svg') }}">
    <span>Consumer</span>
  </div>
 
  <div class="registr_button" onclick="setModalLogin('supplier', '{{ $method }}', '{{ $method }}');">
    <img src="{{ asset('img/icon-shipping-blue.svg') }}">
    <span>Supplier</span>
  </div>
 
  <div class="registr_button" onclick="setModalLogin('brand', '{{ $method }}', '{{ $method }}');">
    <img src="{{ asset('img/icon-shirt-blue.svg') }}">
    <span>Brand</span>
  </div>

  <div class="link_go_back move_link">
    <a href="javascript:void(0);" data-dismiss="modal">Return to homepage</a>
  </div>
@elseif($action == 'login')
  <div class="modal-header">
    <div class="modal-header_img">
      <img src="{{ asset('img/logo.svg') }}">
    </div>
    <h2 class="modal-title" id="exampleModalLabel">Log into your account</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
    <form method="POST" action="{{ route('login') }}" id="formLogin">
      @csrf
      <div class="form-group">
        <label for="recipient-name" class="col-form-label">Email Address:</label>
        <input type="text" class="form-control" id="recipient-name" placeholder="Your email" name="email">
      </div>
      <div class="form-group">
        <label for="message-text" class="col-form-label">Choose a Password:</label>
        <input type="password" class="form-control" id="message-text" placeholder="Password" name="password">
      </div>

      @if($role == 'consumer')<a href="javascript:void(0);" onclick="openNewWindow('{{ route('facebook.auth', $role) }}');"><i class="fa fa-facebook" aria-hidden="true"></i></a>@endif
      <a href="javascript:void(0);" onclick="openNewWindow('{{ route('google.auth', $role) }}');"><i class="fa fa-google" aria-hidden="true"></i></a>
      <a href="javascript:void(0);" onclick="openNewWindow('{{ route('linkedin.auth', $role) }}');"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    </form>
  <div class="modal-footer">
    <button type="button" onclick="$('#formLogin').submit();" class="btn btn-primary">Sign In</button>
  </div>
  <div class="register_go_back">
      <p>Don’t have an account yet?</p>
      <a href="" data-dismiss="modal" data-toggle="modal" data-target="#form3">Register</a>
  </div>
  <div class="link_go_back">
    <a href="javascript:void(0);" onclick="setModalLogin('consumer', 'list', 'login');">Go back</a>
  </div>
@elseif($action == 'register')
  <div class="modal-header">
    <div class="modal-header_img">
      <img src="{{ asset('img/logo.svg') }}">
    </div>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
 
  <div class="registr_soc-icon">
    <a href="javascript:void(0);" onclick="openNewWindow('{{ route('facebook.auth', $role) }}');"><img src="{{ asset('img/facebook.svg') }}"></a>
  </div>
  <div class="registr_soc-icon">
    <a href="javascript:void(0);" onclick="openNewWindow('{{ route('google.auth', $role) }}');"><img src="{{ asset('img/Google.svg') }}"></a>
  </div>
  <div class="registr_soc-icon">
    <a href="javascript:void(0);" onclick="openNewWindow('{{ route('linkedin.auth', $role) }}');"><img src="{{ asset('img/LinkedIn.svg') }}"></a>
  </div>
  <div class="or-divider font-s_16">or</div>
  <div class="registr_button min-btn-email"  href="javascript:void(0);" onclick="setModalLogin('{{ $role }}', 'registerStep', 'register');">
    <img src="{{ asset('img/envelope.svg') }}"> Continue with Email
  </div>

  <div class="link_go_back">
    <a href="/">Return to homepage</a>
  </div>
@elseif($action == 'registerStep')
  <div class="modal-header">
    <div class="modal-header_img">
      <img src="{{ asset('img/logo.svg') }}">
    </div>
    <h2 class="modal-title tt-capitalize" id="exampleModalLabel">Register {{ $role }}</h2>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="close_{{ $role }}">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <form id="reg_{{ $role }}">
    @csrf
    <div class="form-group">
      <label for="email" class="col-form-label">Email Address:</label>
      <input type="text" class="form-control" id="email" placeholder="Your email" name="email">
      <div class="reg_error_{{ $role }}_email reg-error-box"></div>
    </div>
    <div class="form-group">
      <label for="pass" class="col-form-label">Choose a Password:</label>
      <input type="password" class="form-control" id="pass" placeholder="Password" name="password">
      <div class="reg_error_{{ $role }}_password reg-error-box"></div>
    </div>
    <div class="form-group">
      <label for="confirm-pass" class="col-form-label">Confirm Password:</label>
      <input type="password" class="form-control" id="confirm-pass" placeholder="Confirm Password" name="password_confirmation">
    </div>
    <input type="hidden" name="role" value="{{ $role }}">
    <div class="modal-footer">
      <button type="submit" class="btn btn-primary">Register</button>
    </div>
  </form>
  <div class="register_go_back">
      <p>Don’t have an account yet?</p>
      <a href="javascript:void(0);" onclick="setModalLogin('consumer', 'list', 'register');">Register</a>
  </div>
  <div class="link_go_back">
    <a href="javascript:void(0);" onclick="setModalLogin('supplier', '{{ $method }}', '{{ $method }}');">Go back</a>
  </div>
@endif