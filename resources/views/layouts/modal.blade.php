<!-- Form Login -->
<div class="modal fade" id="form1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content" id="modal-login-result">
      {!! Core::getModalLoginList(); !!}
    </div>
  </div>
</div>

<!-- Success -->
<div class="modal fade" id="form2" tabindex="-1" role="dialog" aria-labelledby="form2" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-header_img">
          <img src="{{ asset('img/logo.svg') }}">
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <img src="{{ asset('img/Success.svg') }}">
        <h2 class="modal-title font36" style="padding-top: 25px">Success!</h2>
        <img src="{{ asset('img/Rectangle.svg') }}">
      <div class="register_go_back text_on_fast">
          <p>You are now registered. Please <br>check your inbox to verify <br>your email address.</p>
      </div>
      <div class="link_go_back margin-bt25">
        <a href="/">Return to homepage</a>
      </div>
    </div>
  </div>
</div>

<button type="button" id="good_modal" style="display: none;" data-toggle="modal" data-target="#form2"></button>