@extends('layouts.app')

@section('content')

<section class="grey-bg default-gap">
    <div class="urgent_page">
        <div class="home_brand">
            <a href="{{ route('welcome') }}">Home</a>
            <div class="top_small_border"></div>
            <a href="{{ route('brends') }}">Brand Search</a>
            <div class="top_small_border"></div>
            <span>{{ $brand->name }}</span>
        </div>
    	<div class="box-white_brends one_of_many">
            <div class="brend_image-box image-box_corrected">
                <img src="{{ asset('img/no-image-brends.svg') }}">
            </div>
            <div class="brend_title-box title-box_corrected">
                <h2>{{ $brand->name }}</h2>
                <span class="green-text font-s_12 pd-top-bot_10"><img src="{{ asset('img/checklist.svg') }}" class="checklist-m"> Verified Brand</span>
                <span class="grey-text font-s_12">{{ $brand->country }}, {{ $brand->city }}</span>
                <div class="numb_categ font-s_12">
                    <ul>
                       <li><span>#7 in </span><a href="#">Luxury</a></li>
                       <li><span>#7 in </span><a href="#">Luxury</a></li>
                       <li><span>#7 in </span><a href="#">Luxury</a></li>
                       <li><span>#7 in </span><a href="#">Luxury</a></li>
                       <li><span>#7 in </span><a href="#">Luxury</a></li>
                    </ul>
                </div>
            </div>
            <div class="star_field">
                <div class="brend_customer-box width50">
                    <b class="font-s_12">Customer Rating</b>
                    @if($brand->star_customer)
                        <b class="count-marg">{{ number_format($brand->star_customer, 2, '.', '') }}</b>
                    @else
                        <b class="count-marg">N/A</b>
                    @endif
                    <div class="rating">
                        <div class="stars like_rate">
                            <div class="on" style="width: {{ $brand->star_customer * 20 }}%;"></div>
                        </div>
                    </div>
                    <span class="grey-text font-s_12">{{ Reviews::where('brand_id', $brand->id)->where('role', 'consumer')->count() }} reviews</span>
                </div>
                <div class="brend_supplier-box width50">
                    <b class="font-s_12">Supplier Rating</b>
                    @if($brand->star_supplier)
                        <b class="count-marg">{{ number_format($brand->star_supplier, 2, '.', '') }}</b>
                    @else
                        <b class="count-marg">N/A</b>
                    @endif
                    <div class="rating">
                        <div class="stars">
                            <div class="on" style="width: {{ $brand->star_supplier * 20 }}%;"></div>
                        </div>
                    </div>
                    <span class="grey-text font-s_12">{{ Reviews::where('brand_id', $brand->id)->where('role', 'supplier')->count() }} reviews</span>
                </div>
                <div class="brend_button-box button-box_corrected">
                    <a href="{{ route('write.review.brand', $brand->name) }}" class="btn primary">LEAVE A REVIEW</a>
                </div>
            </div>
        </div>
    
        <div class="tabs_stars">
            <nav class="nav-title-tabs">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-customerreviews-tab" data-toggle="tab" href="#nav-customerreviews" role="tab" aria-controls="nav-customerreviews" aria-selected="true">customer reviews</a>
                    <span class="strip_in_tab"></span>
                    <a class="nav-item nav-link" id="nav-supplierreviews-tab" data-toggle="tab" href="#nav-supplierreviews" role="tab" aria-controls="nav-supplierreviews" aria-selected="false">supplier reviews</a>
                    <span class="strip_in_tab"></span>
                    <a class="nav-item nav-link" id="nav-compare-tab" data-toggle="tab" href="#nav-compare" role="tab" aria-controls="nav-compare" aria-selected="false">compare</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-customerreviews" role="tabpanel" aria-labelledby="nav-customerreviews-tab">
                    <div class="customer_reviews_bloc">
                        <div class="average_ratings">
                            <h4>Average Ratings</h4>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Overall Rating</span>
                                </div>
                                <div class="stars_with_points">
                                    <div class="rating">
                                        <div class="stars like_rate">
                                            <div class="on" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <span>4.0</span>
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Purchased online</span>
                                </div>
                                <div class="stars_with_points">
                                    50%
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Think {{ $brand->name }} is a lead in their product category</span>
                                </div>
                                <div class="stars_with_points">
                                    10%
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                Most common words used to describe {{ $brand->name }}
                            </div>
                            <div class="reviews_in_line df-er">
                                <b>Trendy</b><b>Trendy</b><b>Trendy</b>
                            </div>
                        </div>
                        <div class="region pad-r-45">
                            <h4>Region</h4>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="africa" name="region[]">
                                    <label for="africa">Africa</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="asia" name="region[]" checked>
                                    <label for="asia">Asia</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="europe" name="region[]" checked>
                                    <label for="europe">Europe</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="north america" name="region[]">
                                    <label for="north america">North America</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="oceania" name="region[]">
                                    <label for="oceania">Oceania</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="south america" name="region[]">
                                    <label for="south america">South America</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                        </div>
                        <div class="date_of_review">
                            <h4>Date of review</h4>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="all time" name="date[]">
                                    <label for="all time">All time</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="this year" name="date[]" checked>
                                    <label for="this year">This year</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="this month" name="date[]">
                                    <label for="this month">This month</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="this week" name="date[]">
                                    <label for="this week">This week</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>                      
                        </div>
                    </div>
                    <div class="most_recent">
                        <div class="most_recent1">
                            <span>300 Customer Reviews for {{ $brand->name }}</span>
                        </div>
                        <div class="most_recent2">
                            <div class="block-sort_by most_recent3">
                                <span>Sort by:</span>
                                <div class="drop-box_m box_m_corrected">
                                    <span><span class="rename_id rename_id_corrected font-s_18">Most Recent</span> <img src="{{ asset('img/chevron-down-blue.svg') }}" class="arrow-drop transition-block"></span>
                                </div>
                                <div class="drop-down_wrap">
                                    <ul>
                                        <li class="drop-index transition-block">Most Recent</li>
                                        <li class="drop-index transition-block">Highest Rating</li>
                                        <li class="drop-index transition-block">Lowest Rating</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="different_reviews">
                        <div class="different_reviews1">
                            <div class="photo_name_stars">
                                <div class="photo_name_date">
                                    <div class="avatar_different_reviews">
                                        <img src="{{ asset('img/user-grey.svg') }}">
                                    </div>
                                    <div class="time_and_place">
                                        <div class="username">
                                            <span class="font-s_20 bold_name">John Smith</span>
                                            <span class="number_contributions">(7 contributions)</span>
                                        </div>
                                        <div class="username_data">
                                            <span>Singapore 4 May 2020</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="rating">
                                    <div class="stars like_rate rating_right">
                                        <div class="on" style="width: 80%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="review_text">
                                <h4 class="font-s_20 green-text">Good Products, but had to que for a long time</h4>
                                <span>I really like Valentinos products and have shopped there for years but recently I have noticed that there are not enough staff to help at the checkouts. The design of the products also seems to be changing.</span>
                            </div>
                            <div class="numbers_up_bottom">
                                <div class="hand_up">
                                    <img src="{{ asset('img/like-blue.svg') }}">
                                    <span>22</span>
                                </div>
                                <div class="hand_down">
                                    <img src="{{ asset('img/dislike-grey.svg') }}">
                                    <span>0</span>
                                </div>
                            </div>
                        </div>
                        <div class="different_reviews1" style="padding-top: 30px">
                            <div class="photo_name_stars">
                                <div class="photo_name_date">
                                    <div class=" avatar_different_reviews">
                                        <img src="{{ asset('img/user-grey.svg') }}">
                                    </div>
                                    <div class="time_and_place">
                                        <div class="username">
                                            <span class="font-s_20 bold_name">John Smith</span>
                                            <span class="number_contributions">(4 contributions)</span>
                                        </div>
                                        <div class="username_data">
                                            <span>Paris, France 4 May 2020</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="rating">
                                    <div class="stars like_rate">
                                        <div class="on" style="width: 20%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="review_text">
                                <h4 class="font-s_20 text_red">Good Products, but had to que for a long time</h4>
                                <span>N/A</span>
                            </div>
                            <div class="numbers_up_bottom">
                                <div class="hand_up">
                                    <img src="{{ asset('img/like-grey.svg') }}">
                                    <span>0</span>
                                </div>
                                <div class="hand_down">
                                    <img src="{{ asset('img/dislike-red.svg') }}">
                                    <span>4</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="double_mw_btn">
                        <div class="to_indent"></div>
                        <div class="show_more">
                            Show more reviews
                            <img src="{{ asset('img/chevron-down-blue.svg') }}">
                        </div>
                        <div class="brend_button-box tweaked_button-box">
                            <a href="{{ route('write.review.brand', $brand->name) }}" class="btn primary">LEAVE A REVIEW</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-supplierreviews" role="tabpanel" aria-labelledby="nav-supplierreviews-tab">
                    <div class="customer_reviews_bloc">
                        <div class="average_ratings">
                            <h4>Average Ratings</h4>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Fair price and terms</span>
                                </div>
                                <div class="stars_with_points">
                                    <div class="rating">
                                        <div class="stars">
                                            <div class="on" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <span>4.0</span>
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Planning</span>
                                </div>
                                <div class="stars_with_points">
                                    <div class="rating">
                                        <div class="stars">
                                            <div class="on" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <span>4.0</span>
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Communication</span>
                                </div>
                                <div class="stars_with_points">
                                    <div class="rating">
                                        <div class="stars">
                                            <div class="on" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <span>4.0</span>
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Timely Payment</span>
                                </div>
                                <div class="stars_with_points">
                                    <div class="rating">
                                        <div class="stars">
                                            <div class="on" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <span>4.0</span>
                                </div>
                            </div>
                            <div class="reviews_in_line">
                                <div class="review_classification">
                                    <span>Partnership quality</span>
                                </div>
                                <div class="stars_with_points">
                                    <div class="rating">
                                        <div class="stars">
                                            <div class="on" style="width: 80%;"></div>
                                        </div>
                                    </div>
                                    <span>4.0</span>
                                </div>
                            </div>
                        </div>
                        <div class="region pad-r-45">
                            <h4>Region</h4>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="africa" name="region[]">
                                    <label for="africa">Africa</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="asia" name="region[]" checked>
                                    <label for="asia">Asia</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="europe" name="region[]" checked>
                                    <label for="europe">Europe</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="north america" name="region[]">
                                    <label for="north america">North America</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="oceania" name="region[]">
                                    <label for="oceania">Oceania</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="south america" name="region[]">
                                    <label for="south america">South America</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                        </div>
                        <div class="date_of_review">
                            <h4>Date of review</h4>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="all time" name="date[]">
                                    <label for="all time">All time</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="this year" name="date[]" checked>
                                    <label for="this year">This year</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="this month" name="date[]">
                                    <label for="this month">This month</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>
                            <div class="continent">
                                <div class="continent22">
                                    <input type="checkbox" id="this week" name="date[]">
                                    <label for="this week">This week</label>
                                </div>
                                <div class="continent33">
                                    <span>(999)</span>
                                </div>
                            </div>                      
                        </div>
                    </div>
                    <div class="most_recent">
                        <div class="most_recent1">
                            <span>300 Customer Reviews for {{ $brand->name }}</span>
                        </div>
                        <div class="most_recent2">
                            <div class="block-sort_by most_recent3">
                                <span>Sort by:</span>
                                <div class="drop-box_m box_m_corrected">
                                    <span><span class="rename_id rename_id_corrected font-s_18">Most Recent</span> <img src="{{ asset('img/chevron-down-blue.svg') }}" class="arrow-drop transition-block"></span>
                                </div>
                                <div class="drop-down_wrap">
                                    <ul>
                                        <li class="drop-index transition-block">Most Recent</li>
                                        <li class="drop-index transition-block">Highest Rating</li>
                                        <li class="drop-index transition-block">Lowest Rating</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="different_reviews">
                        <div class="different_reviews1">
                            <div class="photo_name_stars">
                                <div class="photo_name_date">
                                    <div class="avatar_different_reviews">
                                        <img src="{{ asset('img/user-grey.svg') }}">
                                    </div>
                                    <div class="time_and_place">
                                        <div class="username">
                                            <span class="font-s_20 bold_name">John Smith</span>
                                            <span class="number_contributions">(7 contributions)</span>
                                        </div>
                                        <div class="username_data">
                                            <span>Singapore 4 May 2020</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="rating">
                                    <div class="stars rating_right">
                                        <div class="on" style="width: 80%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="review_text">
                                <h4 class="font-s_20 green-text">Good Products, but had to que for a long time</h4>
                                <span>I really like Valentinos products and have shopped there for years but recently I have noticed that there are not enough staff to help at the checkouts. The design of the products also seems to be changing.</span>
                            </div>
                            <div class="numbers_up_bottom">
                                <div class="hand_up">
                                    <img src="{{ asset('img/like-blue.svg') }}">
                                    <span>22</span>
                                </div>
                                <div class="hand_down">
                                    <img src="{{ asset('img/dislike-grey.svg') }}">
                                    <span>0</span>
                                </div>
                            </div>
                        </div>
                        <div class="different_reviews1" style="padding-top: 30px">
                            <div class="photo_name_stars">
                                <div class="photo_name_date">
                                    <div class=" avatar_different_reviews">
                                        <img src="{{ asset('img/user-grey.svg') }}">
                                    </div>
                                    <div class="time_and_place">
                                        <div class="username">
                                            <span class="font-s_20 bold_name">John Smith</span>
                                            <span class="number_contributions">(4 contributions)</span>
                                        </div>
                                        <div class="username_data">
                                            <span>Paris, France 4 May 2020</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="rating">
                                    <div class="stars">
                                        <div class="on" style="width: 20%;"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="review_text">
                                <h4 class="font-s_20 text_red">Good Products, but had to que for a long time</h4>
                                <span>N/A</span>
                            </div>
                            <div class="numbers_up_bottom">
                                <div class="hand_up">
                                    <img src="{{ asset('img/like-grey.svg') }}">
                                    <span>0</span>
                                </div>
                                <div class="hand_down">
                                    <img src="{{ asset('img/dislike-red.svg') }}">
                                    <span>4</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="double_mw_btn">
                        <div class="to_indent"></div>
                        <div class="show_more">
                            Show more reviews
                            <img src="{{ asset('img/chevron-down-blue.svg') }}">
                        </div>
                        <div class="brend_button-box tweaked_button-box">
                            <a href="{{ route('write.review.brand', $brand->name) }}" class="btn primary">LEAVE A REVIEW</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-compare" role="tabpanel" aria-labelledby="nav-compare-tab">
                    compare
                </div>
            </div>
        </div>
    </div>
</section>
@endsection