<div class="review-box-white">
    <div class="box-for-step-review">
        <div class="left-box-step">
            <span class="font-s_16">1</span>
        </div>
        <div class="right-box-step">
            <h4>Write a Consumer or Supplier review?</h4>
            <div class="ds-block-none-content">
                @if(Auth::guest())
                    <div class="box-for-choice_button consumer-bt transition-block">
                        Consumer
                    </div>
                    <div class="box-for-choice_button supplier-bt transition-block">
                        Supplier
                    </div>
                @else
                    <div class="box-for-choice_button consumer-bt transition-block @if(Auth::user()->role != 'consumer') auth-click-no @endif">
                        Consumer
                    </div>
                    <div class="box-for-choice_button consumer-bt transition-block @if(Auth::user()->role != 'supplier') auth-click-no @endif">
                        Supplier
                    </div>
                @endif
            </div>
        </div>
    </div>

    <div class="box-for-step-review index-step_last">
        <div class="left-box-step">
            <span class="font-s_16">2</span>
        </div>
        <div class="right-box-step">
            <h4>Write a Review for</h4>
            <div class="ds-block-none-content">

                <!-- Consumer -->
                <form class="form-consumer transition-block form-toogle" method="POST" action="{{ route('customerReviews.add', $brand) }}">
                    @csrf
                    <input type="hidden" name="role" value="consumer">
                    <input type="hidden" name="brand" value="{{ $brand->id }}">

                    <div class="brand-box-review">
                        <div class="brand-box-review_left">
                            <img src="{{ asset('img/no-image-brends.svg') }}">
                        </div>
                        <div class="brand-box-review_right">
                            <h2>{{ $brand->name }}</h2>
                            <span class="grey-text font-s_12">{{ $brand->country }}, {{ $brand->city }}</span>
                            <ul class="products_tags font-s_12">
                                <li>Luxury</li>
                                <li>Menswear</li>
                                <li>Womenswear</li>
                                <li>Bags</li>
                                <li>Accessories</li>
                                <li>Shoes</li>
                            </ul>
                        </div>
                    </div>

                    <table class="table-for_field">
                        <tr>
                            <td><label for="anonymous">I wish to remain anonymous:</label></td>
                            <td><input type="checkbox" name="anonymous" id="anonymous"></td>
                        </tr>
                        <tr>
                            <td><label for="purchased-products">I have purchased products from <span>{{ $brand->name }}</span> before <span class="required_s">*</span></label></td>
                            <td><input type="checkbox" name="purchased_products" id="purchased-products" required></td>
                        </tr>
                    </table>

                    <div class="box-p_two-col">
                        <p>I purchased: <span class="required_s">*</span></p>
                        <div class="box-p_two-col_label">
                            <label><input type="checkbox" name="i_purchased" value="Menswear" required>Menswear</label>
                            <label><input type="checkbox" name="i_purchased" value="Bags" required>Bags</label>
                            <label><input type="checkbox" name="i_purchased" value="Womenswear" required>Womenswear</label>
                            <label><input type="checkbox" name="i_purchased" value="Accessories" required>Accessories</label>
                            <label><input type="checkbox" name="i_purchased" value="Kidswear" required>Kidswear</label>
                            <label><input type="checkbox" name="i_purchased" value="Shoes" required>Shoes</label>
                        </div>
                    </div>

                    <div class="box-p_two-col">
                        <p>I purchased products: <span class="required_s">*</span></p>
                        <div class="box-p_two-col_label">
                            <label><input type="checkbox" name="i_purchased_products" value="Online" required>Online</label>
                            <label><input type="checkbox" name="i_purchased_products" value="In a store" required>In a store</label>
                        </div>
                    </div>

                    <h4 class="gap-form-title">One word I would use to describe <span>{{ $brand->name }}’s</span> products: <span class="required_s">*</span></h4>
                    <input class="min-input" type="text" name="use_to_describe" placeholder="Type here one word">
                    <div class="box-for-rd-grn">
                        <div class="rd-grn_left">
                            <p>Or select one:</p>
                        </div>
                        <div class="rd-grn_right">
                            <div class="rd-grn_row">
                                <label class="not-standard-label grn-f"><input type="radio" name="use_to_describe" value="Comfortable" required><span>Comfortable</span></label>
                                <label class="not-standard-label grn-f"><input type="radio" name="use_to_describe" value="Trendy" required><span>Trendy</span></label>
                                <label class="not-standard-label grn-f"><input type="radio" name="use_to_describe" value="Classy" required><span>Classy</span></label>
                                <label class="not-standard-label grn-f"><input type="radio" name="use_to_describe" value="Inspiring" required><span>Inspiring</span></label>
                                <label class="not-standard-label grn-f"><input type="radio" name="use_to_describe" value="Cool" required><span>Cool</span></label>
                            </div>
                            <div class="rd-grn_row">
                                <label class="not-standard-label red-f"><input type="radio" name="use_to_describe" value="Uncomfortable" required><span>Uncomfortable</span></label>
                                <label class="not-standard-label red-f"><input type="radio" name="use_to_describe" value="Boring" required><span>Boring</span></label>
                                <label class="not-standard-label red-f"><input type="radio" name="use_to_describe" value="Uninspiring" required><span>Uninspiring</span></label>
                                <label class="not-standard-label red-f"><input type="radio" name="use_to_describe" value="Basic" required><span>Basic</span></label>
                            </div>
                        </div>
                    </div>

                    <table class="table-for_field">
                        <tr>
                            <td><label for="good-value-for-money">I think {{ $brand->name }}’s products are <strong>good value for money</strong> <span class="required_s">*</span></label></td>
                            <td><input type="checkbox" name="good_value_for_money" value="good value for money" id="good-value-for-money" required></td>
                        </tr>
                        <tr>
                            <td><label for="leader-in-their-product">I think {{ $brand->name }}’s is a <strong>leader</strong> in their product category <span class="required_s">*</span></label></td>
                            <td><input type="checkbox" name="leader_in_their_product" value="leader in their product" id="leader-in-their-product" required></td>
                        </tr>
                        <tr class="margin-bot_50">
                            <td><label for="eco-friendly">I think {{ $brand->name }} is <strong>eco-friendly</strong> and <strong>people friendly</strong> <span class="required_s">*</span></label></td>
                            <td><input type="checkbox" name="eco_friendly" value="eco-friendly and people friendly" id="eco-friendly" required></td>
                        </tr>
                        <tr>
                            <td><label for="using-website">I enjoy using {{ $brand->name }}’s <strong>website</strong> <span class="required_s">*</span></label></td>
                            <td><input type="checkbox" name="using_website" value="enjoy-using-{{ $brand->name }}-website" id="using-website" required></td>
                        </tr>
                        <tr>
                            <td><label for="establishments">I enjoy visiting {{ $brand->name }} <strong>establishments</strong> <span class="required_s">*</span></label></td>
                            <td><input type="checkbox" name="establishments" value="establishments" id="establishments" required></td>
                        </tr>
                    </table>

                    <h4 class="gap-form-title min-bottom-mrg">What I love about {{ $brand->name }} is: <span class="optional_txt">(optional)</span></h4>
                    <textarea name="love_about_brend" placeholder="Maximum 500 characters" maxlength="500"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">What I dislike about {{ $brand->name }} is: <span class="optional_txt">(optional)</span></h4>
                    <textarea name="dislike_about_brend" placeholder="Maximum 500 characters" maxlength="500"></textarea>

                    <table class="table-for_field top-gap">
                        <tr>
                            <td><label for="i-would-refer">I would refer {{ $brand->name }} to a friend <span class="required_s">*</span></label></td>
                            <td class="right_algn"><input type="checkbox" name="i_would_refer" id="i-would-refer" value="i-would-refer" required></td>
                        </tr>
                        <tr>
                            <td><label for="i-would-shop">I would shop at {{ $brand->name }} again <span class="required_s">*</span></label></td>
                            <td class="right_algn"><input type="checkbox" name="i_would_shop" id="i-would-shop" value="i-would-shop" required></td>
                        </tr>
                        <tr>
                            <td><label>Overall Rating: <span class="required_s">*</span></label></td>
                            <td>
                                <div class="overall-rating-area">
                                    <div class="pd-left"><strong><span class="count-overall-rating">0</span>/5</strong></div>
                                    <input type="radio" name="radio_rating" value="5" id="overall-rat_5" required>
                                    <label for="overall-rat_5" title="5" class="index-lb-rat"></label>
                                    <input type="radio" name="radio_rating" value="4" id="overall-rat_4" required>
                                    <label for="overall-rat_4" title="4" class="index-lb-rat"></label>
                                    <input type="radio" name="radio_rating" value="3" id="overall-rat_3" required>
                                    <label for="overall-rat_3" title="3" class="index-lb-rat"></label>
                                    <input type="radio" name="radio_rating" value="2" id="overall-rat_2" required>
                                    <label for="overall-rat_2" title="2" class="index-lb-rat"></label>
                                    <input type="radio" name="radio_rating" value="1" id="overall-rat_1" required>
                                    <label for="overall-rat_1" title="1" class="index-lb-rat"></label>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <h4 class="gap-form-title min-bottom-mrg">Please tell us more about your experience? <span class="optional_txt">(optional)</span></h4>
                    <textarea name="tell_us_more_about_your_experience" placeholder="Maximum 1,000 characters" maxlength="1000"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">Title of your Review? <span class="optional_txt">(optional)</span></h4>
                    <textarea name="title_of_your_review" placeholder="Maximum 100 characters" maxlength="100"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">Submit your review <span class="required_s">*</span></h4>
                    <label class="flex-col_pr-pol font-s_12"><input type="checkbox" name="privacy_policy" value="yes" required><span>I certify that the information entered in this review is based on my own experience and is my genuine opinion of this Brand and that I have no personal or business relationship with this Company, and have not been offered any incentive or payment originating form the establishment to write this review. I understand that Labelled.co has a zero-tolerance policy on fake reviews. <a href="#">Learn more</a></span></label>

                    <button type="submit" class="btn primary">Submit review</button>
                </form>

                <!-- Supplier -->
                <form class="form-supplier transition-block form-toogle" method="POST" action="{{ route('customerReviews.add', $brand) }}">
                    @csrf
                    <input type="hidden" name="role" value="supplier">
                    <input type="hidden" name="brand" value="{{ $brand->id }}">

                    <div class="brand-box-review">
                        <div class="brand-box-review_left">
                            <img src="{{ asset('img/no-image-brends.svg') }}">
                        </div>
                        <div class="brand-box-review_right">
                            <h2>{{ $brand->name }}</h2>
                            <span class="grey-text font-s_12">{{ $brand->country }}, {{ $brand->city }}</span>
                            <ul class="products_tags font-s_12">
                                <li>Luxury</li>
                                <li>Menswear</li>
                                <li>Womenswear</li>
                                <li>Bags</li>
                                <li>Accessories</li>
                                <li>Shoes</li>
                            </ul>
                        </div>
                    </div>

                    <h4 class="gap-form-title min-bottom-mrg">Title of your Review? <span class="required_s">*</span></h4>
                    <textarea name="title_of_your_review" placeholder="Maximum 100 characters" maxlength="100" required></textarea>

                    <table class="table-for_field top-bottom-gap">
                        <tr>
                            <td><label for="remain-anonymous">I wish to remain anonymous:<p class="grey-text font-s_12">We will still ask for certain information to verify your review. Learn more</p></label></td>
                            <td><input type="checkbox" name="remain_anonymous" id="remain-anonymous" value="yes"></td>
                        </tr>
                    </table>

                    <table class="table-for_field top-bottom-gap">
                        <tr>
                            <td><label for="name_of_your_company">Name of your Company: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="name_of_your_company" id="name_of_your_company" placeholder="Company Name" required></td>
                        </tr>
                        <tr>
                            <td><label for="company_address">Company Address: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="company_address" id="company_address" placeholder="Street Address" required></td>
                        </tr>
                        <tr>
                            <td><label for="city">City: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="city" id="city" placeholder="City" required></td>
                        </tr>
                        <tr>
                            <td><label for="state">State: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="state" id="state" placeholder="State" required></td>
                        </tr>
                        <tr>
                            <td><label for="country">Country: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="country" id="country" placeholder="Country" required></td>
                        </tr>
                        <tr>
                            <td><label for="postcode">Postcode: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="postcode" id="postcode" placeholder="Postcode" required></td>
                        </tr>
                        <tr>
                            <td><label for="phone_number">Phone Number: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="phone_number" id="phone_number" placeholder="Phone Number" required></td>
                        </tr>
                        <tr>
                            <td><label for="website">Website: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="website" id="website" placeholder="Website" required></td>
                        </tr>
                        <tr>
                            <td><label>Your Name: <span class="required_s">*</span></label></td>
                            <td class="two-fields_m"><input type="text" name="first_name" placeholder="First Name" class="forst_m" required><input type="text" name="last_name" placeholder="Last Name" class="last_m" required></td>
                        </tr>
                        <tr>
                            <td><label for="position_in_company">Position in Company: <span class="required_s">*</span></label></td>
                            <td><input type="text" name="position_in_company" id="position_in_company" placeholder="Position in Company" required></td>
                        </tr>
                        <tr>
                            <td><label for="how_many_years">How many Years have you been supplying products to {{ $brand->name }}? <span class="required_s">*</span></label></td>
                            <td><input type="text" name="how_many_years" id="how_many_years" required></td>
                        </tr>
                        <tr>
                            <td><label for="do_you_supply_directly">Do you supply directly to {{ $brand->name }} or through a third party? <span class="required_s">*</span></label></td>
                            <td><input type="text" name="do_you_supply_directly" id="do_you_supply_directly" required></td>
                        </tr>
                    </table>

                    <div class="box-p_two-col">
                        <p>Which Product Categories do you manufacture for {{ $brand->name }}? <span class="required_s">*</span></p>
                        <div class="box-p_two-col_label">
                            <label><input type="checkbox" name="which_product_categories_do" value="Menswear" required>Menswear</label>
                            <label><input type="checkbox" name="which_product_categories_do" value="Bags" required>Bags</label>
                            <label><input type="checkbox" name="which_product_categories_do" value="Womenswear" required>Womenswear</label>
                            <label><input type="checkbox" name="which_product_categories_do" value="Accessories" required>Accessories</label>
                            <label><input type="checkbox" name="which_product_categories_do" value="Kidswear" required>Kidswear</label>
                            <label><input type="checkbox" name="which_product_categories_do" value="Shoes" required>Shoes</label>
                        </div>
                    </div>
                    
                    <h4 class="gap-form-title min-bottom-mrg">Please upload a document as proof of your relationship with {{ $brand->name }}. The document must not be older than 12 months <span class="required_s">*</span></h4>
                    <p class="grey-text font-s_12">E.g. Purchase Order, Email, Cost Sheet, etc. This document will be kept Private and Confidential, Click here to learn more.</p>
                    <input type="file" name="document_file" class="top-gap-min" required>

                    <h4 class="gap-form-title min-bottom-mrg">Please describe the contents of the uploaded document <span class="required_s">*</span></h4>
                    <textarea type="text" name="contents_of_the_uploaded-document" placeholder="Describe uploaded document"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">Please rate {{ $brand->name }}: <span class="required_s">*</span></h4>
                    <table class="table-for_field top-gap">
                        <tr>
                            <td><label>Fair Price and Payment Terms <span class="required_s">*</span></label></td>
                            <td>
                                <div class="padding-left-table">
                                    <div class="overall-rating-area stars-rating-style">
                                        <div class="pd-left"><strong><span class="count-overall-rating">0</span>/5</strong></div>
                                        <input type="radio" name="fair_price" value="5" id="fair_price_5" required>
                                        <label for="fair_price_5" title="5" class="index-lb-rat"></label>
                                        <input type="radio" name="fair_price" value="4" id="fair_price_4" required>
                                        <label for="fair_price_4" title="4" class="index-lb-rat"></label>
                                        <input type="radio" name="fair_price" value="3" id="fair_price_3" required>
                                        <label for="fair_price_3" title="3" class="index-lb-rat"></label>
                                        <input type="radio" name="fair_price" value="2" id="fair_price_2" required>
                                        <label for="fair_price_2" title="2" class="index-lb-rat"></label>
                                        <input type="radio" name="fair_price" value="1" id="fair_price_1" required>
                                        <label for="fair_price_1" title="1" class="index-lb-rat"></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="grey-text font-s_12 other-indents_m">(0 = not fair, 3 = satisfactory, 5 = very fair)</td>
                        </tr>
                        <tr>
                            <td><label>Planning <span class="required_s">*</span></label></td>
                            <td>
                                <div class="padding-left-table">
                                    <div class="overall-rating-area stars-rating-style">
                                        <div class="pd-left"><strong><span class="count-overall-rating">0</span>/5</strong></div>
                                        <input type="radio" name="planning" value="5" id="planning_5" required>
                                        <label for="planning_5" title="5" class="index-lb-rat"></label>
                                        <input type="radio" name="planning" value="4" id="planning_4" required>
                                        <label for="planning_4" title="4" class="index-lb-rat"></label>
                                        <input type="radio" name="planning" value="3" id="planning_3" required>
                                        <label for="planning_3" title="3" class="index-lb-rat"></label>
                                        <input type="radio" name="planning" value="2" id="planning_2" required>
                                        <label for="planning_2" title="2" class="index-lb-rat"></label>
                                        <input type="radio" name="planning" value="1" id="planning_1" required>
                                        <label for="planning_1" title="1" class="index-lb-rat"></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="grey-text font-s_12 other-indents_m">(0 = poor planning, 3 = satisfactory, 5 = good planning)</td>
                        </tr>
                        <tr>
                            <td><label>Communication <span class="required_s">*</span></label></td>
                            <td>
                                <div class="padding-left-table">
                                    <div class="overall-rating-area stars-rating-style">
                                        <div class="pd-left"><strong><span class="count-overall-rating">0</span>/5</strong></div>
                                        <input type="radio" name="communication" value="5" id="communication_5" required>
                                        <label for="communication_5" title="5" class="index-lb-rat"></label>
                                        <input type="radio" name="communication" value="4" id="communication_4" required>
                                        <label for="communication_4" title="4" class="index-lb-rat"></label>
                                        <input type="radio" name="communication" value="3" id="communication_3" required>
                                        <label for="communication_3" title="3" class="index-lb-rat"></label>
                                        <input type="radio" name="communication" value="2" id="communication_2" required>
                                        <label for="communication_2" title="2" class="index-lb-rat"></label>
                                        <input type="radio" name="communication" value="1" id="communication_1" required>
                                        <label for="communication_1" title="1" class="index-lb-rat"></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="grey-text font-s_12 other-indents_m">(0 = poor communication, 3 = satisfactory, 5 = good communication)</td>
                        </tr>
                        <tr>
                            <td><label>Timely Payment <span class="required_s">*</span></label></td>
                            <td>
                                <div class="padding-left-table">
                                    <div class="overall-rating-area stars-rating-style">
                                        <div class="pd-left"><strong><span class="count-overall-rating">0</span>/5</strong></div>
                                        <input type="radio" name="timely_payment" value="5" id="timely_payment_5" required>
                                        <label for="timely_payment_5" title="5" class="index-lb-rat"></label>
                                        <input type="radio" name="timely_payment" value="4" id="timely_payment_4" required>
                                        <label for="timely_payment_4" title="4" class="index-lb-rat"></label>
                                        <input type="radio" name="timely_payment" value="3" id="timely_payment_3" required>
                                        <label for="timely_payment_3" title="3" class="index-lb-rat"></label>
                                        <input type="radio" name="timely_payment" value="2" id="timely_payment_2" required>
                                        <label for="timely_payment_2" title="2" class="index-lb-rat"></label>
                                        <input type="radio" name="timely_payment" value="1" id="timely_payment_1" required>
                                        <label for="timely_payment_1" title="1" class="index-lb-rat"></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="grey-text font-s_12 other-indents_m">(0 = never pay on time, 3 = sometimes pay on time, 5 = always pay on time)</td>
                        </tr>
                        <tr>
                            <td><label>Quality of Partnership <span class="required_s">*</span></label></td>
                            <td>
                                <div class="padding-left-table">
                                    <div class="overall-rating-area stars-rating-style">
                                        <div class="pd-left"><strong><span class="count-overall-rating">0</span>/5</strong></div>
                                        <input type="radio" name="quality_of_partnership" value="5" id="quality_of_partnership_5" required>
                                        <label for="quality_of_partnership_5" title="5" class="index-lb-rat"></label>
                                        <input type="radio" name="quality_of_partnership" value="4" id="quality_of_partnership_4" required>
                                        <label for="quality_of_partnership_4" title="4" class="index-lb-rat"></label>
                                        <input type="radio" name="quality_of_partnership" value="3" id="quality_of_partnership_3" required>
                                        <label for="quality_of_partnership_3" title="3" class="index-lb-rat"></label>
                                        <input type="radio" name="quality_of_partnership" value="2" id="quality_of_partnership_2" required>
                                        <label for="quality_of_partnership_2" title="2" class="index-lb-rat"></label>
                                        <input type="radio" name="quality_of_partnership" value="1" id="quality_of_partnership_1" required>
                                        <label for="quality_of_partnership_1" title="1" class="index-lb-rat"></label>
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="grey-text font-s_12 other-indents_m">(0 = poor, 3 = satisfactory, 5 = amazing)</td>
                        </tr>
                    </table>

                    <h4 class="gap-form-title min-bottom-mrg">Please tell us more about your experience? <span class="optional_txt">(optional)</span></h4>
                    <textarea name="tell_us_more_about_your_experience" placeholder="Maximum 1,000 characters" maxlength="1000"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">What could {{ $brand->name }} do better? <span class="optional_txt">(optional)</span></h4>
                    <textarea name="what_could_do_better" placeholder="Maximum 250 characters" maxlength="250"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">What is something {{ $brand->name }} could do that would surprise you? <span class="optional_txt">(optional)</span></h4>
                    <textarea name="could_do_that_would_surprise_you" placeholder="Maximum 250 characters" maxlength="250"></textarea>

                    <h4 class="gap-form-title min-bottom-mrg">Submit your review <span class="required_s">*</span></h4>
                    <label class="flex-col_pr-pol font-s_12"><input type="checkbox" name="privacy_policy" value="yes" required><span>I certify that the information entered in this review is based on my own experience and is my genuine opinion of this Brand and that I have no personal or business relationship with this Company, and have not been offered any incentive or payment originating form the establishment to write this review. I understand that Labelled.co has a zero-tolerance policy on fake reviews. <a href="#">Learn more</a></span></label>

                    <button type="submit" class="btn primary">Submit review</button>
                </form>

            </div>
        </div>
    </div>

    @if(Auth::guest())
        <div class="box-for-step-review index-step_3">
            <div class="left-box-step">
                <span class="font-s_16">3</span>
            </div>
            <div class="right-box-step">
                <h4>Login/Register to leave a review</h4>
                <div class="ds-block-none-content">

                </div>
            </div>
        </div>
    @endif
</div>