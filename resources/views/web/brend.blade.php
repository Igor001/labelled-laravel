@extends('layouts.app')

@section('content')

<input type="hidden" id="current-page" value="1">

<section class="grey-bg default-gap">
    <div class="container">

        <div class="row">
            <div class="my-col_m my-col-filter">
            </div>
            <div class="my-col_m my-col-content search100">
                <div class="mb-search_group-block">
                    <input type="text" list="m-search" class="search_inp" name="q" id="">
                    <button type="submit" value="" class="mb-search-button">go</button>   
                </div>
            </div>
            <div class="my-col_m my-col-add">
            </div>
        </div>

        <div class="row top-pd_20 top-pd_0">
            <div class="my-col_m my-col-filter">
            </div>
            <div class="my-col_m my-col-content search100">
                <div class="row flex-al_item_center my-flex-block_mc">
                    <div class="div-col">
                        <div class="font-s_20 bottom_pd_35" id="results_text"></div>
                    </div>
                    <div class="div-col">
                        <div class="block-sort_by">
                            <b>Sort by:</b>
                            <div class="drop-box_m">
                                <span><span class="rename_id">Most Relevant</span> <img src="{{ asset('img/chevron-down.svg') }}" class="arrow-drop transition-block"></span>
                            </div>
                            <div class="drop-down_wrap">
                                <ul>
                                    <li class="drop-index transition-block sort-action" data-sort="1">Most Relevant</li>
                                    <li class="drop-index transition-block sort-action" data-sort="2">Highest Rating: Customer</li>
                                    <li class="drop-index transition-block sort-action" data-sort="3">Highest Rating: Supplier</li>
                                    <li class="drop-index transition-block sort-action" data-sort="4">Lowest Rating: Customer</li>
                                    <li class="drop-index transition-block sort-action" data-sort="5">Lowest Rating: Supplier</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="my-col_m my-col-add">
            </div>
        </div>

        <div class="row top-pd_34 top-pd_0">
            <div class="my-col_m my-col-filter adapted_filter">
                <form method="POST" id="filter_form" action="/filter">
                    @csrf
                    <input type="hidden" name="sort" id="sortData">
                    <div class="white-box_with_border padding-f-box index-class-box">
                        <div class="filter-title">
                            <div class="filtr-img_h">
                                <img src="{{ asset('img/filter.svg') }}">
                                <h4 class="uppercase">Filters</h4>
                            </div>
                            <div class="reset-filter">
                                <b class="color-red">Reset</b>
                            </div>
                        </div>
                        <div class="four_filter_ms_box">
                            <div class="filter_ms_box">    
                                <button class="title-trg_filter font-s_20" type="button" data-toggle="collapse" data-target="#collproducts" aria-expanded="true" aria-controls="collproducts"><b>Products</b> <img src="{{ asset('img/chevron-down.svg') }}" class="arrow-drop transition-block"></button>
                                <div class="collapse show" id="collproducts">
                                    <div class="card-body_filter">
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="" class="index-cls-checkbox">
                                                <span>Shoes</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">(9999)</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="" class="index-cls-checkbox">
                                                <span>Luxury</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">(9999)</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="" class="index-cls-checkbox">
                                                <span>Menswear</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">(9999)</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="filter_ms_box">
                                <button class="title-trg_filter font-s_20" type="button" data-toggle="collapse" data-target="#collcountry" aria-expanded="true" aria-controls="collcountry"><b>Country</b> <img src="{{ asset('img/chevron-down.svg') }}" class="arrow-drop transition-block"></button>
                                <div class="collapse show" id="collcountry">
                                    <div class="card-body_filter">
                                        @foreach($brends as $brend)
                                            <label class="filters-label">
                                                <div class="left-inp">
                                                    <input type="checkbox" name="country[]" value="{{ $brend->country }}" class="filter-country index-cls-checkbox">
                                                    <span>{{ $brend->country }}</span>
                                                </div>
                                                <div class="right-count">
                                                    <span class="grey-text font-s_16">({{ Brend::where('country', $brend->country)->count() }})</span>
                                                </div>
                                            </label>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            
                            <div class="filter_ms_box">
                                <button class="title-trg_filter font-s_20" type="button" data-toggle="collapse" data-target="#collcosrate" aria-expanded="true" aria-controls="collcosrate"><b>Customer Rating</b> <img src="{{ asset('img/chevron-down.svg') }}" class="arrow-drop transition-block"></button>
                                <div class="collapse show" id="collcosrate">
                                    <div class="card-body_filter">
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="likes[five]" class="index-cls-checkbox filter-likes" value="5">
                                                <span>5 likes</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountLikes(5) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="likes[four]" class="index-cls-checkbox filter-likes" value="4">
                                                <span>4 likes</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountLikes(4) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="likes[three]" class="index-cls-checkbox filter-likes" value="3">
                                                <span>3 likes</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountLikes(3) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="likes[two]" class="index-cls-checkbox filter-likes" value="2">
                                                <span>2 likes</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountLikes(2) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="likes[one]" class="index-cls-checkbox filter-likes" value="1">
                                                <span>1 like</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountLikes(1) }})</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="filter_ms_box">
                                <button class="title-trg_filter font-s_20" type="button" data-toggle="collapse" data-target="#collsupsrate" aria-expanded="false" aria-controls="collsupsrate"><b>Supplier Rating</b> <img src="{{ asset('img/chevron-down.svg') }}" class="arrow-drop transition-block"></button>
                                <div class="collapse" id="collsupsrate">
                                    <div class="card-body_filter">
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="stars[five]" class="index-cls-checkbox filter-stars" value="5">
                                                <span>5 stars</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountStars(5) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="stars[four]" class="index-cls-checkbox filter-stars" value="4">
                                                <span>4 stars</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountStars(4) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="stars[three]" class="index-cls-checkbox filter-stars" value="3">
                                                <span>3 stars</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountStars(3) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="stars[two]" class="index-cls-checkbox filter-stars" value="2">
                                                <span>2 stars</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountStars(2) }})</span>
                                            </div>
                                        </label>
                                        <label class="filters-label">
                                            <div class="left-inp">
                                                <input type="checkbox" name="stars[one]" class="index-cls-checkbox filter-stars" value="1">
                                                <span>1 star</span>
                                            </div>
                                            <div class="right-count">
                                                <span class="grey-text font-s_16">({{ Brend::getCountStars(1) }})</span>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="my-col_m my-col-content adapted_content">
                <div id="loading_box" style="display: none; text-align: center;">
                    <div class="spinner-grow text-warning" role="status">
                        <span class="sr-only">Loading...</span>
                    </div>
                </div>
                <div id="result_box">
                    {!! $returnHTML !!}
                </div>

                <div class="show-more-brends">
                    <div class="show-more-wrp">
                        <span class="font-s_14" id="showMore">Show more reviews</span>
                        <img src="{{ asset('img/chevron-down-blue.svg') }}">
                    </div>
                </div>
            </div>
            <div class="my-col_m my-col-add">
                <img src="{{ asset('img/add.png') }}" class="add-img">
            </div>
        </div>

    </div>
</section>

@endsection
