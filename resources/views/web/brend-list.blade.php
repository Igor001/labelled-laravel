@foreach($results as $brend)

	<div class="box-white_brends">
        <div class="brend_image-box">
            <img src="{{ asset('img/no-image-brends.svg') }}">
        </div>
        <div class="brend_title-box">
            <a href="{{ route('customerReviews', $brend->name) }}" class="brand-name-def"><h2>{{ $brend->name }}</h2></a>
            <span class="grey-text font-s_12">{{ $brend->country }}, {{ $brend->city }}</span>
            <div class="numb_categ font-s_12">
                <ul>
                   <li><span>#7 in </span><a href="#">Luxury</a></li>
                   <li><span>#7 in </span><a href="#">Luxury</a></li>
                   <li><span>#7 in </span><a href="#">Luxury</a></li>
                   <li><span>#7 in </span><a href="#">Luxury</a></li>
                   <li><span>#7 in </span><a href="#">Luxury</a></li>
                </ul>
            </div>
        </div>
        <div class="brend_customer-box">
            <b class="font-s_12">Customer likes</b>
            @if($brend->star_customer)
                <b class="count-marg">{{ number_format($brend->star_customer, 2, '.', '') }}</b>
            @else
                <b class="count-marg">N/A</b>
            @endif
            <div class="rating">
                <div class="stars like_rate">
                    <div class="on" style="width: {{ $brend->star_customer * 20 }}%;"></div>
                </div>
            </div>
            <span class="grey-text font-s_12">{{ Reviews::where('brand_id', $brend->id)->where('role', 'consumer')->count() }} reviews</span>
        </div>
        <div class="brend_supplier-box">
            <b class="font-s_12">Supplier Rating</b>
            @if($brend->star_supplier)
                <b class="count-marg">{{ number_format($brend->star_supplier, 2, '.', '') }}</b>
            @else
                <b class="count-marg">N/A</b>
            @endif
            <div class="rating">
                <div class="stars">
                    <div class="on" style="width: {{ $brend->star_supplier * 20 }}%;"></div>
                </div>
            </div>
            <span class="grey-text font-s_12">{{ Reviews::where('brand_id', $brend->id)->where('role', 'supplier')->count() }} reviews</span>
        </div>
        <div class="brend_button-box">
            <a href="{{ route('customerReviews', $brend->name) }}" class="btn primary_border margin-bot_16">Visit Profile</a>
            <a href="{{ route('write.review.brand', $brend->name) }}" class="btn primary">Write a review</a>
        </div>
    </div>

@endforeach