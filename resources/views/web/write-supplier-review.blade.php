@extends('layouts.app')

@section('content')

<form enctype="multipart/form-data">
	<label>Title of your Review? * <input type="text" name="title_of_your_review" placeholder="Maximum 100 characters"></label>
	<label>I wish to remain anonymous: * <input type="checkbox" name="remain_anonymous" value="yes"></label>
	<label>Name of your Company: * <input type="text" name="name_of_your_company" placeholder="Company Name"></label>

	<label>Company Address: * <input type="text" name="company_address" placeholder="Street Address"></label>
	<label>City: * <input type="text" name="city" placeholder="City"></label>
	<label>State: * <input type="text" name="state" placeholder="State"></label>
	<label>Country: * <input type="text" name="country" placeholder="Country"></label>
	<label>Postcode: * <input type="text" name="postcode" placeholder="Postcode"></label>
	<label>Phone Number: * <input type="text" name="phone_number" placeholder="Phone Number"></label>
	<label>Website: * <input type="text" name="website" placeholder="Website"></label>
	<label>Your Name: * <input type="text" name="first_name" placeholder="First Name"><input type="text" name="last-name" placeholder="Last Name"></label>
	<label>Position in Company: * <input type="text" name="position_in_company" placeholder="Position in Company"></label>

	<label>How many Years have you been supplying products to Valentino? * <input type="text" name="how_many_years"></label>
	<label>Do you supply directly to Valentino or through a third party? * <input type="text" name="do_you_supply_directly"></label>

	<p>Which Product Categories do you manufacture for Valentino? * </p>
	<label><input type="checkbox" name="which_product_categories_do[menswear]" value="Menswear">Menswear</label>
	<label><input type="checkbox" name="which_product_categories_do[bags]" value="Bags">Bags</label>
	<label><input type="checkbox" name="which_product_categories_do[womenswear]" value="Womenswear">Womenswear</label>
	<label><input type="checkbox" name="which_product_categories_do[accessories]" value="Accessories">Accessories</label>
	<label><input type="checkbox" name="which_product_categories_do[kidswear]" value="Kidswear">Kidswear</label>
	<label><input type="checkbox" name="which_product_categories_do[shoes]" value="Shoes">Shoes</label>

	<p>Please upload a document as proof of your relationship with Valentino. The document must not be older than 12 months * <span>E.g. Purchase Order, Email, Cost Sheet, etc. This document will be kept Private and Confidential, Click here to learn more.</span></p>
	<input type="file" name="file">
	<label>Please describe the contents of the uploaded document: * <input type="text" name="contents_of_the_uploaded_document"></label>

	<p>Please rate Valentino: *</p>
	<div>
		<p>Fair Price and Payment Terms <span>(0 = not fair, 3 = satisfactory, 5 = very fair)</span></p>
		<label><input type="radio" name="fair_price_and_payment_terms[one]" value="1">1</label>
		<label><input type="radio" name="fair_price_and_payment_terms[two]" value="2">2</label>
		<label><input type="radio" name="fair_price_and_payment_terms[three]" value="3">3</label>
		<label><input type="radio" name="fair_price_and_payment_terms[four]" value="4">4</label>
		<label><input type="radio" name="fair_price_and_payment_terms[five]" value="5">5</label>
	</div>
	<div>
		<p>Planning <span>(0 = poor planning, 3 = satisfactory, 5 = good planning)</span></p>
		<label><input type="radio" name="planning[one]" value="1">1</label>
		<label><input type="radio" name="planning[two]" value="2">2</label>
		<label><input type="radio" name="planning[three]" value="3">3</label>
		<label><input type="radio" name="planning[four]" value="4">4</label>
		<label><input type="radio" name="planning[five]" value="5">5</label>
	</div>
	<div>
		<p>Communication <span>(0 = poor communication, 3 = satisfactory, 5 = good communication)</span></p>
		<label><input type="radio" name="сommunication[one]" value="1">1</label>
		<label><input type="radio" name="сommunication[two]" value="2">2</label>
		<label><input type="radio" name="сommunication[three]" value="3">3</label>
		<label><input type="radio" name="сommunication[four]" value="4">4</label>
		<label><input type="radio" name="сommunication[five]" value="5">5</label>
	</div>
	<div>
		<p>Timely Payment <span>(0 = never pay on time, 3 = sometimes pay on time, 5 = always pay on time)</span></p>
		<label><input type="radio" name="timely_payment[one]" value="1">1</label>
		<label><input type="radio" name="timely_payment[two]" value="2">2</label>
		<label><input type="radio" name="timely_payment[three]" value="3">3</label>
		<label><input type="radio" name="timely_payment[four]" value="4">4</label>
		<label><input type="radio" name="timely_payment[five]" value="5">5</label>
	</div>
	<div>
		<p>Quality of Partnership <span>(0 = poor, 3 = satisfactory, 5 = amazing)</span></p>
		<label><input type="radio" name="quality_of_partnership[one]" value="1">1</label>
		<label><input type="radio" name="quality_of_partnership[two]" value="2">2</label>
		<label><input type="radio" name="quality_of_partnership[three]" value="3">3</label>
		<label><input type="radio" name="quality_of_partnership[four]" value="4">4</label>
		<label><input type="radio" name="quality_of_partnership[five]" value="5">5</label>
	</div>

	<label>Please tell us more about your experience? <span>(optional)</span> <textarea name="tell_us_more_about_your_experience" placeholder="Maximum 1,000 characters"></textarea></label>
	<label>What could Valentino do better? <span>(optional)</span> <textarea name="what_could_do_better" placeholder="Maximum 250 characters"></textarea></label>
	<label>What is something Valentino could do that would surprise you? <span>(optional)</span> <textarea name="could_do_that_would_surprise_you" placeholder="Maximum 250 characters"></textarea></label>

	<p>Submit your review *</p>
	<label><input type="checkbox" name="i_certify_that_the_information" value="yes">I certify that the information entered in this review is based on my own experience and is my genuine opinion of this Brand and that I have no personal or business relationship with this Company, and have not been offered any incentive or payment originating form the establishment to write this review. I understand that Labelled.co has a zero-tolerance policy on fake reviews. Learn more</label>

	<button type="submit">Submit review</button>
</form>

@endsection