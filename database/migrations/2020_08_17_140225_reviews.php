<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->integer('user');
            $table->integer('star')->nullable();
            $table->longText('text')->nullable();
            $table->integer('date')->nullable();
            $table->string('role');
            $table->integer('brand_id');

            $table->string('anonymous')->nullable();
            $table->string('purchased_products')->nullable();
            $table->string('i_purchased')->nullable();
            $table->string('i_purchased_products')->nullable();
            $table->string('use_to_describe')->nullable();
            $table->string('good_value_for_money')->nullable();
            $table->string('leader_in_their_product')->nullable();
            $table->string('eco_friendly')->nullable();
            $table->string('using_website')->nullable();
            $table->string('establishments')->nullable();
            $table->string('love_about_brend')->nullable();
            $table->string('dislike_about_brend')->nullable();
            $table->string('i_would_refer')->nullable();
            $table->string('i_would_shop')->nullable();
            $table->string('radio_rating')->nullable();
            $table->string('tell_us_more_about_your_experience')->nullable();
            $table->string('privacy_policy')->nullable();

            $table->string('title_of_your_review')->nullable();
            $table->string('remain_anonymous')->nullable();
            $table->string('name_of_your_company')->nullable();
            $table->string('company_address')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('postcode')->nullable();
            $table->string('phone_number')->nullable();
            $table->string('website')->nullable();
            $table->string('first_name')->nullable();
            $table->string('position_in_company')->nullable();
            $table->string('how_many_years')->nullable();
            $table->string('do_you_supply_directly')->nullable();
            $table->string('which_product_categories_do')->nullable();
            $table->string('file')->nullable();
            $table->string('contents_of_the_uploaded_document')->nullable();
            $table->string('fair_price_and_payment_terms')->nullable();
            $table->string('planning')->nullable();
            $table->string('сommunication')->nullable();
            $table->string('timely_payment')->nullable();
            $table->string('quality_of_partnership')->nullable();
            $table->string('what_could_do_better')->nullable();
            $table->string('could_do_that_would_surprise_you')->nullable();
            $table->string('i_certify_that_the_information')->nullable();

            $table->integer('status')->default(0);
            $table->string('geo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}
